defmodule Bowling do
  require Bowling.Check, as: Check

  def new() do
    []
  end

  def roll(game, pins) do
    [pins | game]
  end

  def score(game) do
    game
    |> Enum.reverse()
    |> reduce_2_at_a_time()
  end

  defp reduce_2_at_a_time(game, score \\ 0) do
    case game do
      [10, p2, p3] ->
        last_frame_strike(score, p2, p3)

      [p1, p2, p3] when Check.is_spare(p1, p2) ->
        last_frame_spare(score, p3)


      [10 | [p2, p3 | _] = game] ->
        strike(game, score, p2, p3)

      [p1, p2 | [p3 | _] = game] when Check.is_spare(p1, p2) ->
        spare(game, score, p3)


      [p1, p2 | game] ->
        reduce_2_at_a_time(game, score + p1 + p2)

      _ ->
        Enum.reduce(game, score, &(&1 + &2))

    end
  end


  defp strike(game, score, p2, p3) do
    reduce_2_at_a_time(game, score + 10 + p2 + p3)
  end

  defp spare(game, score, p3) do
    reduce_2_at_a_time(game, score + 10 + p3)
  end

  defp last_frame_strike(score, p2, p3) do
    score + 10 + p2 + p3
  end
  defp last_frame_spare(score, p3) do
    score + 10 + p3
  end
end
