defmodule Bowling do
  require Bowling.Check, as: Check
  require InspectVars

  def new() do
    []
  end

  def roll(game, pins) do
    # InspectVars.inspect([game, pins])
    # [pins | game]
  # end

  # # Note: We fill in the game backwards, always adding new rolls to front of
  # #   array (for Elixir effcient'ness). Remember to reverse before scoreing
  def score(game) do
    game
    |> Enum.reverse()
    |> reduce_1_frame_at_a_time(0)
  end


  defp reduce_1_frame_at_a_time(game, score) do
    case game do
      [] ->
        score

      [pins] ->
        score + pins

      [p1, p2] ->
        score + p1 + p2

      [10, p2, p3] ->
        strike_on_last_frame(score, p2, p3)


      [10, p2, p3 | _] ->
        strike(game, score, p2, p3)

      [p1, p2 | game] when Check.is_spare(p1, p2) ->
        spare(game, score, p1, p2)

      [p1, p2 | game] ->
        reduce_1_frame_at_a_time(game, score + p1 + p2)
    end
  end

  defp strike_on_last_frame(score, p2, p3) do
    score + 10 + p2 + p3
  end

  defp strike(game, score, p2, p3) do
    reduce_1_frame_at_a_time(List.delete_at(game, 0), score + 10 + p2 + p3)
  end

  defp spare(game, score, p1, p2) do
    reduce_1_frame_at_a_time(game, score + p1 + p2 + List.first(game))
  end
end
