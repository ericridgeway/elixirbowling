defmodule BowlingTest do
  use ExUnit.Case
  doctest Bowling

  #
  # .score tests
  #
  test "Can score all-0s game" do
    all_0_game = roll_many(Bowling.new(), 0, 20)

    assert Bowling.score(all_0_game) == 0
  end

  test "Can score all-1s game" do
    all_1_game = roll_many(Bowling.new(), 1, 20)

    assert Bowling.score(all_1_game) == 20
  end

  test "Can score 1 spare" do
    score =
      Bowling.new()
      |> roll_spare()
      |> Bowling.roll(3)
      |> roll_many(0, 17)
      |> Bowling.score()

    assert score == 16
  end

  test "Can score 1 strike" do
    score =
      Bowling.new()
      |> roll_strike()
      |> Bowling.roll(3)
      |> Bowling.roll(4)
      |> roll_many(0, 16)
      |> Bowling.score()

    assert score == 24
  end

  test "Can score all-strikes" do
    score =
      Bowling.new()
      |> roll_many(10, 12)
      |> Bowling.score()

    assert score == 300
  end


  #
  # Misc tests
  #
  test ".new- Can make a new empty game" do
    assert Bowling.new() == []
  end

  test ".roll- Can roll and it adds to the game" do
    # Note: Rolls add to front of game list. Doesn't reverse until score/*
    game =
      Bowling.new()
      |> Bowling.roll(0)
      |> Bowling.roll(1)

    assert game == [1, 0]
  end

  test "<self>.roll_many- Can auto roll an all-n game" do
    all_3_game = roll_many(Bowling.new(), 3, 20)

    assert length(all_3_game) == 20
    assert Bowling.score(all_3_game) == 3 * 20
  end


  #
  # Private functions
  #
  defp roll_spare(game) do
    game
    |> Bowling.roll(5)
    |> Bowling.roll(5)
  end

  defp roll_strike(game) do
    Bowling.roll(game, 10)
  end

  defp roll_many(game, _, _ = 0), do: game
  defp roll_many(game, pins, i) do
    game
    |> Bowling.roll(pins)
    |> roll_many(pins, i - 1)
  end

end
