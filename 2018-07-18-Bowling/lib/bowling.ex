defmodule Bowling do
  require Bowling.Check, as: Check

  def new() do
    []
  end

  def roll(game, pins) do
    [pins | game]
  end

  def score(game) do
    game
    |> Enum.reverse()
    |> reduce_1_frame_at_a_time()
  end


  defp reduce_1_frame_at_a_time(game, score \\ 0) do
    case game do
      [10, p3, p4] ->
        last_frame_strike(score, p3, p4)

      [p1, p2, p3] when Check.is_spare(p1, p2) ->
        last_frame_spare(score, p3)


      [10, p3 | tail] = game ->
        strike(game, score, p3, tail)

      [p1, p2 | game] when Check.is_spare(p1, p2) ->
        # TODO vim keybind and the cmd alias require IEx;IEx.pry
        spare(game, score)


      [p1, p2 | game] ->
        reduce_1_frame_at_a_time(game, score + p1 + p2)

      [game] ->
        score + Enum.reduce([game], &(&1 + &2))

      [] ->
        score
    end
  end


  defp spare(game, score) do
    reduce_1_frame_at_a_time(game, score + 10 + List.first(game))
  end
  defp strike(game, score, p3, tail) do
    reduce_1_frame_at_a_time(List.delete_at(game, 0), score + 10 + p3 + List.first(tail))
  end

  defp last_frame_strike(score, p3, p4) do
    score + 10 + p3 + p4
  end
  defp last_frame_spare(score, p3) do
    score + 10 + p3
  end
end
