defmodule BowlingTest do
  use ExUnit.Case
  doctest Bowling

  test "gutter game" do
    score =
      Bowling.new()
      |> roll_many(0, 20)
      |> Bowling.score()

    assert score == 0
  end

  test "all 1s" do
    score =
      Bowling.new()
      |> roll_many(1, 20)
      |> Bowling.score()

    assert score == 20
  end

  test "1 spare" do
    score =
      Bowling.new()
      |> spare()
      |> Bowling.roll(3)
      |> roll_many(0, 17)
      |> Bowling.score()

    assert score == 16
  end

  test "1 strike" do
    score =
      Bowling.new()
      |> strike()
      |> Bowling.roll(3)
      |> Bowling.roll(4)
      |> roll_many(0, 16)
      |> Bowling.score()

    assert score == 24
  end

  test "all strikes" do
    score =
      Bowling.new()
      |> roll_many(10, 12)
      |> Bowling.score()

    assert score == 300
  end

  test "last frame spare" do
    score =
      Bowling.new()
      |> roll_many(0, 18)
      |> Bowling.roll(5)
      |> Bowling.roll(5)
      |> Bowling.roll(3)
      |> Bowling.score()

    assert score == 13
  end


  defp roll_many(game, _, 0), do: game
  defp roll_many(game, pins, i) do
    game
    |> Bowling.roll(pins)
    |> roll_many(pins, i - 1)
  end

  defp spare(game) do
    game
    |> Bowling.roll(5)
    |> Bowling.roll(5)
  end

  defp strike(game) do
    Bowling.roll(game, 10)
  end

end
