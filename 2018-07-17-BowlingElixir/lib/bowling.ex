defmodule Bowling do
  require Bowling.Check, as: Check

  def new() do
     []
  end

  def roll(game, pins) do
    [pins | game]
  end

  def score(game) do
    game
    |> Enum.reverse()
    |> reduce_2_at_a_time()
  end


  defp reduce_2_at_a_time(game, score \\ 0) do
    case game do
      [] ->
        score
      [p1] ->
        score + p1
      [p1, p2] ->
        score + p1 + p2

      [10, p1, p2] ->
        last_frame_strike(score, p1, p2)

      [p1, p2, p3] when Check.is_spare(p1, p2) ->
        last_frame_spare(score, p3)


      [10, p1 | tail] = game ->
        strike(game, score, p1, tail)

      [p1, p2 | game] when Check.is_spare(p1, p2) ->
        spare(game, score)

      [p1, p2 | game] ->
        reduce_2_at_a_time(game, score + p1 + p2)
    end
  end

  defp strike(game, score, p1, tail) do
    reduce_2_at_a_time(List.delete_at(game, 0), score + 10 + p1 + List.first(tail))
  end

  defp spare(game, score) do
    reduce_2_at_a_time(game, score + 10 + List.first(game))
  end

  defp last_frame_strike(score, p1, p2) do
    score + 10 + p1 + p2
  end

  defp last_frame_spare(score, p3) do
    score + 10 + p3
  end
end
