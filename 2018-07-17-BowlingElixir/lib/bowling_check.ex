defmodule Bowling.Check do
  defguard is_spare(p1, p2) when p1 + p2 == 10
end
