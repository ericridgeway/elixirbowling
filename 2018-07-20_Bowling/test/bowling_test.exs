defmodule BowlingTest do
  use ExUnit.Case
  doctest Bowling

  test "gutter game" do
    score =
      Bowling.new()
      |> roll_loop_n_times(0, 20)
      |> Bowling.score()

    assert score == 0
  end

  test "all 1s" do
    score =
      Bowling.new()
      |> roll_loop_n_times(1, 20)
      |> Bowling.score()

    assert score == 20
  end


  #
  # Private methods
  #
  defp roll_loop_n_times(game, _, 0), do: game
  defp roll_loop_n_times(game, pins, n) do
    game
    |> Bowling.roll(pins)
    |> roll_loop_n_times(pins, n - 1)
  end
end
