defmodule Bowling do

  def new() do
    []
  end

  def roll(game, pins) do
    [pins | game]
  end

  def score(game) do
    # TODO reverse game before start scoring
    Enum.reduce(game, &(&1 + &2))
  end
end
