defmodule Bowling do
  require Bowling.Check, as: Check

  def new() do
    []
  end

  def roll(game, pins) do
    [pins | game]
  end

  def score(game) do
    game
    |> Enum.reverse()
    |> reduce_2_at_a_time()
  end


  defp reduce_2_at_a_time(game, score \\ 0)

  defp reduce_2_at_a_time([10, p2, p3], score) do
    last_frame_strike(score, p2, p3)
  end
  defp reduce_2_at_a_time([p1, p2, p3], score) when Check.is_spare(p1, p2) do
    last_frame_spare(score, p3)
  end

  defp reduce_2_at_a_time([10 | [p2, p3 | _] = game], score) do
    strike(game, score, p2, p3)
  end
  defp reduce_2_at_a_time([p1, p2 | [p3 | _] = game], score) when Check.is_spare(p1, p2) do
    spare(game, score, p3)
  end

  defp reduce_2_at_a_time([p1, p2 | game], score) do
    reduce_2_at_a_time(game, score + p1 + p2)
  end
  defp reduce_2_at_a_time(game, score) do
    game |> Enum.reduce(score, &(&1 + &2))
  end

  defp strike(game, score, p2, p3) do
    reduce_2_at_a_time(game, score + 10 + p2 + p3)
  end
  defp spare(game, score, p3) do
    reduce_2_at_a_time(game, score + 10 + p3)
  end

  defp last_frame_strike(score, p2, p3) do
    score + 10 + p2 + p3
  end
  defp last_frame_spare(score, p3) do
    score + 10 + p3
  end
end
