defmodule BowlingTest do
  use ExUnit.Case
  doctest Bowling

  test "can roll all 0s" do
    score =
      Bowling.new()
      |> loop_rolls(0, 20)
      |> assert_length(20)
      |> Bowling.score()

    assert score == 0
  end

  test "can roll all 1s" do
    score =
      Bowling.new()
      |> loop_rolls(1, 20)
      |> assert_length(20)
      |> Bowling.score()

    assert score == 20
  end

  test "1 spare" do
    score =
      Bowling.new()
      |> spare()
      |> Bowling.roll(3)
      |> loop_rolls(0, 17)
      |> assert_length(20)
      |> Bowling.score()

    assert score == 16
  end

  test "1 strike" do
    score =
      Bowling.new()
      |> strike()
      |> Bowling.roll(3)
      |> Bowling.roll(4)
      |> loop_rolls(0, 16)
      |> assert_length(20)
      |> Bowling.score()

    assert score == 24
  end

  test "all strikes" do
    score =
      Bowling.new()
      |> loop_rolls(10, 12)
      |> Bowling.score()

    assert score == 300
  end

  test "last frame spare" do
    score =
      Bowling.new()
      |> loop_rolls(0, 18)
      |> spare()
      |> Bowling.roll(4)
      |> assert_length(21)
      |> Bowling.score()

    assert score == 14
  end


  defp assert_length(game, n) do
    game =
      game
      |> add_frames_for_10s()

    assert length(game) == n
    game
  end

  defp add_frames_for_10s(game) do
    num_of_10s =
      game |> Enum.count(&(&1 == 10))

    extra_slots =
      0 |> List.duplicate(num_of_10s)

    game
    |> List.insert_at(0, extra_slots)
    |> List.flatten()
  end

  defp loop_rolls(game, _, 0), do: game
  defp loop_rolls(game, pins, n) do
    game
    |> Bowling.roll(pins)
    |> loop_rolls(pins, n - 1)
  end

  defp spare(game) do
    game
    |> Bowling.roll(5)
    |> Bowling.roll(5)
  end


  defp strike(game) do
    game
    |> Bowling.roll(10)
  end
end
