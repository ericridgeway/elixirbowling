defmodule BowlingTest do
  use ExUnit.Case
  doctest Bowling

  test "can roll all 0s" do
    score =
      Bowling.new()
      |> roll_many(0, 20)
      |> Bowling.score()

    assert score == 0
  end

  test "can roll all 1s" do
    score =
      Bowling.new()
      |> roll_many(1, 20)
      |> Bowling.score()

    assert score == 20
  end

  test "can roll a spare" do
    score =
      Bowling.new()
      |> spare()
      |> Bowling.roll(4)
      |> roll_many(0, 17)
      |> Bowling.score()

    assert score == 18
  end

  test "can roll a strike" do
    score =
      Bowling.new()
      |> strike()
      |> Bowling.roll(1)
      |> Bowling.roll(2)
      |> roll_many(0, 16)
      |> Bowling.score()

    assert score == 16
  end

  test "can roll all strikes" do
    score =
      Bowling.new()
      |> roll_many(10, 12)
      |> Bowling.score()

    assert score == 300
  end

  test "last frame spare" do
    score =
      Bowling.new()
      |> roll_many(0, 18)
      |> Bowling.roll(5)
      |> Bowling.roll(5)
      |> Bowling.roll(4)
      |> Bowling.score()

    assert score == 14
  end


  defp roll_many(game, _, 0), do: game
  defp roll_many(game, pins, n) do
    game
    |> Bowling.roll(pins)
    |> roll_many(pins, n - 1)
  end

  defp strike(game) do
    game
    |> Bowling.roll(10)
  end

  defp spare(game) do
    game
    |> Bowling.roll(5)
    |> Bowling.roll(5)
  end
end
