defmodule Bowling do
  require Bowling.Check, as: Check

  def new() do
    []
  end

  def score(game) do
    game
    |> Enum.reverse()
    |> reduce_2_at_a_time()
  end

  def roll(game, pins) do
    [pins | game]
  end


  defp reduce_2_at_a_time(game, score \\ 0) do
    case game do
      [10, p2, p3] ->
        last_frame_strike(score, p2, p3)

      [p1, p2, p3] when Check.is_spare(p1, p2) ->
        last_frame_spare(score, p3)


      [10 | [p2, p3 | _] = game] ->
        strike(game, p2, p3, score)

      [p1, p2 | [p3 | _] = game] when Check.is_spare(p1, p2) ->
        spare(game, p3, score)


      [p1, p2 | [_ | _] = game] ->
        reduce_2_at_a_time(game, score + p1 + p2)

      _ ->
        score + Enum.reduce(game, 0, &(&1 + &2))
    end
  end


  defp strike(game, p2, p3, score) do
    reduce_2_at_a_time(game, score + 10 + p2 + p3)
  end

  defp spare(game, p3, score) do
    reduce_2_at_a_time(game, score + 10 + p3)
  end


  defp last_frame_strike(score, p2, p3) do
    score + 10 + p2 + p3
  end

  defp last_frame_spare(score, p3) do
    score + 10 + p3
  end
end
